use std::fmt::Display;

pub struct SdlState {
    pub sdl: sdl2::Sdl,
    pub event_pump: sdl2::EventPump,
    pub canvas: sdl2::render::Canvas<sdl2::video::Window>,
}

#[derive(Debug)]
pub struct OtherSdlError(String);

impl Display for OtherSdlError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl std::error::Error for OtherSdlError {}

#[derive(Debug, thiserror::Error)]
pub enum SdlError {
    #[error(transparent)]
    Other(OtherSdlError),
    #[error(transparent)]
    WindowBuild(#[from] sdl2::video::WindowBuildError),
    #[error(transparent)]
    IntegerOrSdl(#[from] sdl2::IntegerOrSdlError),
}

impl From<String> for SdlError {
    fn from(s: String) -> Self {
        Self::Other(OtherSdlError(s))
    }
}

impl SdlState {
    pub fn new(title: &str, width: u32, height: u32) -> Result<Self, SdlError> {
        let sdl = sdl2::init()?;
        let canvas = sdl
            .video()?
            .window(title, width, height)
            .build()?
            .into_canvas()
            .accelerated()
            .present_vsync()
            .build()?;
        let event_pump = sdl.event_pump()?;
        Ok(Self {
            sdl,
            event_pump,
            canvas,
        })
    }
}
