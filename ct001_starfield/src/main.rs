use sdl2::event::Event;

fn main() {
    match run() {
        Ok(()) => {}
        Err(e) => {
            eprintln!("Error: {}", e);
            std::process::exit(1);
        }
    }
}

#[derive(Debug, thiserror::Error)]
enum Error {
    #[error(transparent)]
    Sdl(#[from] ct_common::SdlError),
}

fn run() -> Result<(), Error> {
    let mut state = ct_common::SdlState::new("Starfield", 800, 600)?;

    'main: loop {
        for event in state.event_pump.poll_iter() {
            match event {
                Event::Quit { .. } => break 'main,
                _ => {}
            }
        }

        state.canvas.set_draw_color((51, 51, 51));
        state.canvas.clear();
        state.canvas.present();
    }

    Ok(())
}
